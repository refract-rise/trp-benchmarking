# TRP-benchmarking
### D1.1 Platform and metrics for TRP tools benchmarking, including consensus detection of sequence- and structure-based methods

| Project Acronym:       | REFRACT   |
| :-------------: |:-------------:|
| ||
| Project Full Title:     | REpeat protein Function Refinement, Annotation and Classification of Topologies | 
| Grant Agreement:      | 823886      | 
| Project Duration: | 48 months (01/01/2019 - 31/12/2023) |
| ||
| Work Package:	 | WP1 – Detection |
| Lead Beneficiary | CNRS |

This document describes the Deliverable 1.1 (D1.1) for the work package 1 (WP1) of the REFRACT project. 
The report provides an overview of the algorithm and program scripts developed that make up a software 
for the automatic validation of the results of TR detection in protein sequences. This software will be 
used throughout the WP1 of the REFRACT project by participating project partners who apply different 
computer programs for identification of TRs in amino acid sequences. It will be also used for benchmarking 
of these TR detection tools. As a result, it will be possible to use this computational tool as a uniform 
validator of TR detection for the large scale analyses of TRs in proteomes planned in the WP1 package.  

## List of Acronyms

| Acronym       | Definition   |
| ------------- |-------------|
| 3D     | Three-dimensional | 
| CSRC      | Caucaseco Research Center      | 
| ESR | Early Stage Researcher |
| IDR | Intrinsically Disordered Region |
| MSA | Multiple Sequence Alignment |
| ML | Machine Learning |
| PDB | Protein Data Bank |
| TR | Tandem Repeat |
| TRP | Tandem Repeat Protein |
| WP | Work Package |

## Project overview

### Introduction on the aims
The main purpose of this document is to provide relevant information concerning data and software developed within the framework of D1.1 during the implementation phase of WP1 of the REFRACT action. The aim of the D1.1 to develop a tool able to benchmark existing tools for the detection of TRP regions in protein sequences. TR-containing proteins are abundant, frequently fold in elongated non-globular structures and perform vital functions. A number of computational tools have been developed to detect TRs in protein sequences. A blurred boundary between imperfect TR motifs and non-repetitive sequences gave rise to necessity to validate the detected TRs. In this situation, the 3D structure of proteins can be used as a benchmarking criterion for TR detection in sequences. The majority of proteins having TRs are built of repetitive 3D structural blocks and evolution cannot completely erase the repetitive patterns because some residues located in the equivalent positions of the repeats are critical for maintenance of the stable and functional structure. The partners ambition, in the context of the REFRACT action and WP1.1, is to develop a uniform validator of TR detection for any kind of TR-finders. This will allow us to cover the whole spectrum of TRs and to use this validator for the large scale analyses. 	

### Identification of tasks relevant for the deliverable
D1.1 can be subdivided into three tasks. First, construction of a positive dataset containing “true” TRs and a negative dataset with “false” TRs found both in sequence and in structure. Second, identification of the best set of machine learning features and also selection of the best machine learning (ML) approach. Third, development of a tool that can be used by the researchers for benchmark of TR-detectors. 

## Description of results

### Dataset construction

#### Positive dataset
The positive dataset was constructed by selection of proteins from PDB with TRs in their sequences in accordance with several TR-detecting programs (T-REKS (Jorda and Kajava, 2009), TRUST (Szklarczyk and Heringa, 2004) and HHrepID (Biegert and Söding, 2008). Subsequently, by manual inspection, we selected among these proteins those in which we found TRs both in their sequences and 3D structures. It was complemented by the perfect and almost perfect TRs from proteins having different structural states (stable 3D structure or intrinsically disordered regions (IDRs)). As a result, the positive dataset has 553 multiple sequence alignments (MSAs) of TRs.

#### Negative dataset
The negative dataset was constructed, first, by selecting proteins from PDB with TRs in their sequences in accordance with several TR-detecting programs (T-REKS (Jorda and Kajava, 2009), TRUST (Szklarczyk and Heringa, 2004) and HHrepID (Biegert and Söding, 2008) and, second, by manual inspection among them those proteins which have TRs only in their sequences and not in their 3D structures. It was also enriched in the random aperiodic sequences having different structural states (stable 3D structure or IDR). As a result, the negative set has 525 MSAs of TRs.  	 	

### Machine learning algorithm and features

#### Selection of ML features
The 155 ML features that reflect different characteristics of MSA of TRs (including previously used scoring metrics, gap-opening penalties and Fourier Transform and physico-chemical characteristics of amino acids) were selected. In feature engineering process 55 from total of 155 original attributes were selected for a final model using sequential backward elimination as a feature selection algorithm. The backward feature elimination was done by using H2O.ai platform (2018) and custom implementation in R language. The H2O.ai platform (2018) was used for cross validation process.	

#### Selection of ML algorithm
The TR validator was generated using Random Forest classification ML algorithm, as a method with the best prediction efficacy. Comparison of classifiers generated using different machine learning algorithms, Random Forest, Gradient boosting machine, Generalized linear model and Deep learning was carried out using 10-fold cross-validation of the training set. Predictive performances were estimated by calculating accuracy, area under the receiver operating characteristic curve, area under recall–precision plots, specificity, sensitivity, F1 score, precision and Matthews correlation coefficient.   	

### Development of software for TR validation.
The TR validator, called “Tally-2.0” classifier was implemented in JAVA language using ML platform H2O.ai (2018). As an input, Tally-2.0 uses the list of TR regions presented as MSAs of their repeats. The calculation of MSA based features is implemented in Python and of Spectral features in JAVA. The output lists Tally-2.0 score and several other known TR scores (Psim, entropy, p-value-phylo and parsimony) allowing the users to validate the quality of the examined TRs.

## Availability
Tally-2.0 software is available as a web tool and as a standalone application published under Apache License 2.0, on the URL: https://bioinfo.crbm.cnrs.fr/index.php?route=tools&tool=27. It is supported on Linux. Scripts, codes, documentation, list of dependencies and other elements of the software package are available in GitLab: gitlab.com/refract-rise/trp-benchmarking. External backup of the software package and data is also stored in a local computer of the Lead Beneficiary of D1.1 (CNRS).

## Self-evaluation and concluding remarks
Future perspectives concerning the continuation of improvements on TR-validators will mostly involve enrichment of datasets of “true” and “false” TRs. Several partners of REFRACT project and WP tasks and deliverables will be related to the enrichment of the datasets (especially WP1 - detection, WP2 - characterisation and WP5 - classification). This will open an opportunity for future collaborative work within REFRACT project. The updated datasets will be used to update the TR-validator software.

