# --------------------------- Platform imports ---------------------------------
import subprocess
import os.path

#  ---------------------------- External imports ------------------------------
from pathlib import Path
import pandas as pd



class Tally():
	"""
	Tally is parent class of Launcher class
	This class is used to launch the final steps of global pipeline.
	-Launch Tally Java program on scored features data
	-Write final result with scored features data + Tally score.
	"""

	def merge_result_files(self):
		"""
		-Read and extract the file which contains scored features and the file which contains tally score.
		-Store content of files in dataframe
		-Combine these 2 dataframes in an unique dataframe
		-Write final csv file with the unique dataframe
		-Delete tmp file
		"""
		#ChunkSize to avoid memory crash.
		chunksize=200000
		#Read features scored csv file
		scoring_file = pd.read_csv(self.scoring_output, sep=',', chunksize=chunksize)
		#Read tally score file
		tally_res_file = pd.read_csv(self.tally_output, sep=',', chunksize=chunksize)
		#Combine files
		write_header=True
		for chunk1,chunk2 in zip(scoring_file,tally_res_file):
			merge_res=pd.concat([chunk1, chunk2], axis=1)
			#Column permuation
			merge_res_permuted=merge_res[['Id','MSA','Tally2score','Psim','p-value-phylo','Entropy','Parsimony']]
			#Change column name
			merge_res_permuted.columns=['Id','MSA','Tally-2.0','Psim','p-value-phylo','Entropy','Parsimony']
			#Write result
			merge_res_permuted.to_csv(self.result_path, sep=',', mode='a', encoding='utf-8', index=False,header=write_header)
			write_header=False

		#Delete tmp file
		if Path.exists(Path(self.scoring_output)): Path(self.scoring_output).unlink()
		if Path.exists(Path(self.tally_output)): Path(self.tally_output).unlink()
		if Path.exists(Path(self.tally_input)): Path(self.tally_input).unlink()
		
		

	def run_tally(self):
		"""
		-Launch Tally Java program on compatible tsv file
		-Reformat final output data : merge Tally csv file result (Tally score) with scoring csv file result
		"""
		java_applet_path=str(Path(os.path.dirname(os.path.realpath(__file__))).joinpath("TallyPred.jar"))
		tally_dependencies=str(Path(os.path.dirname(os.path.realpath(__file__))))
		#Launch tally model with quiet mod or not
		if self.quiet_mode:
			subprocess.call(['java','-Xmx4G','-jar', java_applet_path, self.tally_input, self.tally_output, tally_dependencies ,'-h'], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
		else:
			subprocess.call(['java','-Xmx4G','-jar', java_applet_path, self.tally_input, self.tally_output, tally_dependencies ,'-h'])
		
		#Merge results
		self.merge_result_files()
