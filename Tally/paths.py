#!/usr/bin/python3
# -*- coding: utf-8 -*-

from pathlib import Path
import os,sys

DATAROOT = Path(os.path.join(os.path.dirname(sys.argv[0]), "data"))
if not(Path(DATAROOT).exists()) : sys.exit("Missing external resources 'data'")

