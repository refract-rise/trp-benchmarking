# --------------------------- Platform imports ---------------------------------
import os
import re

pat_sequence = re.compile(r'^([-ACDEFGHIKLMNPQRSTVWY]+)$') #valid IUPAC protein codes

class MsaInput:
    """
    This class is for validate MSA sequences from different parser.
    """
    def __init__(self):
        self.__error_list=[]
        self.__repeat_list=[]
        self.__fastally_dico={}
        self.__count_AA=0


    def set_fastally_format(self, p_dictionary):
    	self.fastally_dico=p_dictionary
    
    def get_fastally_format(self):
    	return self.fastally_dico
    
    def set_repeat_list(self, p_repeat):
    	self.__repeat_list.append(p_repeat) 

    def get_repeat_list(self):
    	return self.__repeat_list
    
    def set_error_list(self, p_error):
        self.__error_list.append(p_error)
    
    def get_error_list(self):
        return self.__error_list

    def set_count_AA(self, p_int_value):
        self.__count_AA=p_int_value

    def get_count_AA(self):
        return self.__count_AA

    def validation_fastally(self):
        delete_items_list=[]
        for key in self.fastally_dico.keys():
            value=self.fastally_dico.get(key)
            nb_repeat=len(value)
            if(nb_repeat >= 2):
				#Size of first repeat.
                repeat_length = len(value[0])
                #Check if all MSA sequence have same size
                if not all(len(le) == repeat_length for le in value):
                    delete_items_list.append(key)
                    error=str(key+" : Size between msa repeats are different")
                    self.__error_list.append(str(error))
                join_msa=''.join(value)
                join_msa=join_msa.strip()
                #IUPAC validation
                if not pat_sequence.match(join_msa.upper()):
                    delete_items_list.append(key)
                    error=str(key+" : Invalid character in sequence")
                    self.__error_list.append(str(error))
            else:
                delete_items_list.append(key)
                error=str(key+" : Number of repeat is incorrect")
                self.__error_list.append(str(error))
        for el in delete_items_list:
            del self.fastally_dico[el]

    def validation_others_format(self, p_filename):
        repeat_list=self.__repeat_list
        join_msa=''.join(repeat_list)
        join_msa=join_msa.strip()
        #IUPAC validation
        if not pat_sequence.match(join_msa.upper()):
            error=str(os.path.basename(p_filename)+" : Invalid character in sequence")
            self.__error_list.append(str(error))
            #reset repeat_list
            self.__repeat_list=[]    

    def compute_count_AA(self, p_repeat_list):
        """
        Compute AA number into the list 
        ex : List("CCC","CPC")
        -> CountAA = 6
        """
        msa_text=''.join(p_repeat_list)
        msa_text=msa_text.strip()
        countAA=len(msa_text)-msa_text.count('-')
        self.set_count_AA(countAA)

    def reset_error_list(self):
        self.__error_list=[]

