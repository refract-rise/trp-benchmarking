import os

class WriteOutput:

	@staticmethod
	def create_results_file(p_tally_input_file, p_scoring_output_file):
	    """
	    -Create tsv file with only header
			->This file will be used to store scored features data in Tally compatible format
		-Create csv file with only header.
			->This file will be used to shape the final output results.
	    """
	    #################################################################
	    #		     Tally compatible input format. (TSV file)
	    #################################################################
	    with open(p_tally_input_file, 'w') as f:
	        #header
	        f.write( '\t'.join((
	                'msa','score_pSim','pvalue_pSim','score_entropy','pvalue_entropy','div_phylo','pvalue_phylo',
	                'parsimony','pvalue_parsimony','pSimb45','pSimb62','pSimp120','pSimeq','pSimhydro','pSimSS',
	                'entropyb45','entropyb62','entropyp120','entropyeq','entropyhydro','entropySS','parsimonyb45',
	                'parsimonyb62','parsimonyp120','parsimonyeq','parsimonyhydro','parsimonySS','parsimonyGap',
	                'parsimonyGapO','pSimb45gap','pSimb62gap','pSimp120gap','entropyb45gap','entropyb62gap',
	                'entropyp120gap','parsimonyb45gap','parsimonyb62gap','parsimonyp120gap','msaSTD','nbResidueNorm','nbGapNorm')) + '\n')
	    #################################################################
	    #	   Final scoring result file with only specific column. 
	    #Tally score will be add with merge operation from merge_results.sh
	    #################################################################
	    with open(p_scoring_output_file, 'w') as file:
	    	file.write(','.join(('Id','MSA','Psim','p-value-phylo','Entropy','Parsimony'))+'\n')

	@staticmethod    	
	def write_results_tally_format(poutput_file_name,prot_name, repeats ):
	    """
		Write results on the file created previously (tally_format)
	    """
	    with open(poutput_file_name, 'a') as f:
	        for repeat in repeats:
	            f.write( '\t'.join((
	                repeat.text, #MSA
	                "{:.5}".format(float(repeat.score['pSim'])),
	                "{:.5}".format(float(repeat.pValue['pSim'])),
	                "{:.5}".format(float(repeat.score['entropy'])),
	                "{:.5}".format(float(repeat.pValue['entropy'])),
	                "{:.5}".format(float(repeat.divergence['phylo_gap001'])),
	                "{:.5}".format(float(repeat.pValue['phylo_gap001'])),
	                "{:.5}".format(float(repeat.score['parsimony'])),
	                "{:.5}".format(float(repeat.pValue['parsimony'])),                    
	                "{:.5}".format(float(repeat.score['pSimb45'])),
	                "{:.5}".format(float(repeat.score['pSimb62'])),
	                "{:.5}".format(float(repeat.score['pSimp120'])), 
	                "{:.5}".format(float(repeat.score['pSimeq'])),
	                "{:.5}".format(float(repeat.score['pSimhydro'])),
	                "{:.5}".format(float(repeat.score['pSimSS'])),
	                "{:.5}".format(float(repeat.score['entropyb45'])),
	                "{:.5}".format(float(repeat.score['entropyb62'])),
	                "{:.5}".format(float(repeat.score['entropyp120'])), 
	                "{:.5}".format(float(repeat.score['entropyeq'])),
	                "{:.5}".format(float(repeat.score['entropyhydro'])),
	                "{:.5}".format(float(repeat.score['entropySS'])),
	                "{:.5}".format(float(repeat.score['parsimonyb45'])),
	                "{:.5}".format(float(repeat.score['parsimonyb62'])),
	                "{:.5}".format(float(repeat.score['parsimonyp120'])), 
	                "{:.5}".format(float(repeat.score['parsimonyeq'])),
	                "{:.5}".format(float(repeat.score['parsimonyhydro'])),
	                "{:.5}".format(float(repeat.score['parsimonySS'])),
	                "{:.5}".format(float(repeat.score['parsimonyGap'])),
	                "{:.5}".format(float(repeat.score['parsimonyGapO'])),
	                "{:.5}".format(float(repeat.score['pSimb45gap'])),
	                "{:.5}".format(float(repeat.score['pSimb62gap'])),
	                "{:.5}".format(float(repeat.score['pSimp120gap'])),
	                "{:.5}".format(float(repeat.score['entropyb45gap'])), 
	                "{:.5}".format(float(repeat.score['entropyb62gap'])),
	                "{:.5}".format(float(repeat.score['entropyp120gap'])),
	                "{:.5}".format(float(repeat.score['parsimonyb45gap'])),
	                "{:.5}".format(float(repeat.score['parsimonyb62gap'])),
	                "{:.5}".format(float(repeat.score['parsimonyp120gap'])),
	                str(repeat.msaSTD),
	                "{:.5}".format(float(repeat.score['nbResidueNorm'])),
	                "{:.5}".format(float(repeat.score['nbGapNorm']))
	                )) + '\n')
	            
	@staticmethod
	def write_scoring_results(poutput_file_name,prot_name, repeats):
	    """
		Write results on the file created previously (scored features file)
	    """
	    with open(poutput_file_name, 'a') as f:
	        for repeat in repeats:
	            f.write( ','.join((
	            	os.path.basename(str(prot_name)), #Id
	                repeat.text, #MSA
	                "{:.5}".format(float(repeat.score['pSim'])),
	                "{:.5}".format(float(repeat.pValue['phylo_gap001'])),
	                "{:.5}".format(float(repeat.score['entropy'])),
	                "{:.5}".format(float(repeat.pValue['parsimony']))
	                )) + '\n')
	@staticmethod
	def write_error(p_error_filename, p_input_obj):
		error_list=p_input_obj.get_error_list()
		with open(p_error_filename, 'a') as fi:
			for error in error_list:
				fi.write(str(error) + '\n')
