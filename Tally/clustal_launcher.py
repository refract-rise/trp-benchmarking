#  ---------------------------- External imports ------------------------------
from pathlib import Path
from multiprocessing import Process, JoinableQueue, Lock

#  ---------------------------- External python files ------------------------------
import parser_manager
import output_manager
import input_manager
from scoring import repeat_info

class Clustal:
	"""
	Clustal is parent class of Launcher classes
	This class is used to launch pipeline on clustal format with multiprocessing  
	"""
	
	def start_clustal(self,job_queue,result_lock):
		"""
		-Launch fasta parser
		-Scoring data
		-Write results and error report 
		"""
		try:
			while not job_queue.empty():
				filename=job_queue.get()
				#Display score step if quiet mode is not active
				if self.quiet_mode==False : print(Path(filename).stem)
				#Call a function from parser_manager.py. This function returns an MsaInput object.
				clustal_res_obj=parser_manager.Parser.clustal_parser(filename)
				#Check if list is not empty. (case of format error)
				if(clustal_res_obj.get_repeat_list()):
					#IUPAC sequence validation. 
					clustal_res_obj.validation_others_format(filename)
					clustal_repeat_list=clustal_res_obj.get_repeat_list()
					if(len(clustal_repeat_list)>=2):
						clustal_res_obj.compute_count_AA(clustal_repeat_list)
						countAA=clustal_res_obj.get_count_AA()
						scored_results = [repeat_info.Repeat(msa=clustal_repeat_list,
							begin=1,calc_score = True, calc_pValue = True, end=countAA, passOpti=1)]
						with result_lock:
							output_manager.WriteOutput.write_results_tally_format(self.tally_input,str(filename), scored_results)
							output_manager.WriteOutput.write_scoring_results(self.scoring_output,str(filename), scored_results)
				with result_lock:
					output_manager.WriteOutput.write_error(self.error_filename, clustal_res_obj)
				clustal_res_obj.reset_error_list()
				job_queue.task_done()
		except:
			job_queue.task_done()


	def launch_multiprocessed_clustal(self):
		"""
		-Launch start_clustal() with multiprocessing
			User inputs files will be launched in several distinct processes 
		"""
		result_lock = Lock()
		job_queue = JoinableQueue()
		#Loop the list which contains user files. If file is valid, add file in queue.			
		for filepath in self.file_list:
			file_is_valid=parser_manager.Parser.file_validation(filepath)
			if(file_is_valid):
				job_queue.put(str(filepath))
			else:
				file_error_msg=str(Path(filepath).as_posix() + " : can't open file" )
				print(file_error_msg)
				with open(self.error_filename, 'a') as error_file:
					error_file.write(str(file_error_msg) + '\n')
		#Launch multiprocessing on start_clustal()
		for i in range(self.thread_nb):
			t = Process(
			target=self.start_clustal,
			args=(job_queue ,result_lock)
			)
			t.daemon = True
			t.start()
		job_queue.join()	

