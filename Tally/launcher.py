# --------------------------- Platform imports ---------------------------------
import sys 
import os

#  ---------------------------- External imports ------------------------------
import uuid
from pathlib import Path

#  ---------------------------- External python files ------------------------------
import fastally_launcher 
import tally_launcher
import fasta_launcher
import clustal_launcher

class Launcher(fastally_launcher.Fastally, tally_launcher.Tally, fasta_launcher.Fasta, clustal_launcher.Clustal):
	"""
	Launcher is a child class of Fastally, Fasta, Clustal and Tally classes
	Main class to launch pipeline.
		-User options are stored inside attributes
		-These attributes are used to choose and launch adequate parser
	"""

	#Generate unique tmp filenames
	UNIQUE_FILENAME=str(uuid.uuid4())[0:6]
	#Build path
	CURRENT_PATH=Path(os.path.dirname(os.path.realpath(__file__)))

	
	def __init__(self):
		self._file_list=[]
		self._type_parser=""
		self._error_filename=""
		self._result_path=""
		self._quiet_mode=False
		self._thread_nb=1
		self._chosen_dir=""

	
	
	@property
	def unique_filename(self):
		return self.UNIQUE_FILENAME	
	@property
	def current_path(self):
		return self.CURRENT_PATH
	@property
	def tally_input(self):
		return self.TALLY_INPUT	
	@property
	def tally_output(self):
		return self.TALLY_OUTPUT
	@property
	def scoring_output(self):
		return self.SCORING_OUTPUT
	
	@property
	def file_list(self):
		return self._file_list
	@file_list.setter
	def file_list(self, p_file_list):
		self._file_list=p_file_list
	
	@property
	def type_parser(self):
		return self._type_parser
	@type_parser.setter
	def type_parser(self,p_type_parser):
		self._type_parser=p_type_parser
	
	@property
	def result_path(self):
		return self._result_path
	@result_path.setter
	def result_path(self, p_result_path):
		self._result_path= p_result_path
	
	@property
	def error_filename(self):
		return self._error_filename
	@error_filename.setter
	def error_filename(self, p_error_filename):
		self._error_filename=p_error_filename
	
	@property
	def chosen_dir(self):
		return self._chosen_dir
	@chosen_dir.setter
	def chosen_dir(self, add_path):
		self._chosen_dir=Path(add_path)
	
	@property
	def quiet_mode(self):
		return self._quiet_mode
	@quiet_mode.setter
	def quiet_mode(self, p_quiet_mode):
		self._quiet_mode= p_quiet_mode
		
	@property
	def thread_nb(self):
		return self._thread_nb
	@thread_nb.setter
	def thread_nb(self, p_thread_nb):
		self._thread_nb= p_thread_nb

	def initialize_tally_path(self):
		#Path of tmp files
		self.TALLY_INPUT=str(Path(self._chosen_dir).joinpath("tally_input_{0}.txt".format(self.UNIQUE_FILENAME)))
		self.TALLY_OUTPUT=str(Path(self._chosen_dir).joinpath("out_tally_input_{0}.txt".format(self.UNIQUE_FILENAME)))
		self.SCORING_OUTPUT=str(Path(self._chosen_dir).joinpath("scoring_output_{0}.csv".format(self.UNIQUE_FILENAME)))

	

	def launch_pipeline(self):
		if self.type_parser=="fastally":
			self.launch_multiprocessed_fastally()
		elif self.type_parser=="fasta":
			self.launch_multiprocessed_fasta()
		elif self.type_parser=="clustal":
			self.launch_multiprocessed_clustal()
		else:
			sys.exit("Error : Unknown parser pipeline")
		
		
