# --------------------------- Platform imports ---------------------------------

import math

#  ---------------------------- External imports ------------------------------

import uuid
from pathlib import Path
from multiprocessing import Process, JoinableQueue, Lock



#  ---------------------------- External python files ------------------------------
import parser_manager
import output_manager
import input_manager
from scoring import repeat_info


class Fastally:
	"""
	Fastally is parent class of Launcher class
	This class is used to launch pipeline on fastally format with multiprocessing  
	"""

	def start_fastally(self,job_queue, locked_tools):
		"""
		-Launch fastally parser on files
		-Scoring MSA	
		-Write results and error report 
		"""
		try:
			while not job_queue.empty():
				filename=job_queue.get()
				#This function return an MsaInput object.
				fastally_res=parser_manager.Parser.fastally_parser(filename)			
				fastally_res.validation_fastally()

				#Iterating over dictionary which contains header (key) and MSA (value)
				for keys in fastally_res.get_fastally_format().keys():
					if self.quiet_mode==False : print(keys)
					repeat_fastally=fastally_res.get_fastally_format().get(keys)
					fastally_res.compute_count_AA(repeat_fastally)
					countAA=fastally_res.get_count_AA()
					if(len(repeat_fastally)>1):
						#Launch scoring on msa which is contain into a list
						scored_results = [repeat_info.Repeat(msa=repeat_fastally,
						 begin=1,calc_score = True, calc_pValue = True, end=countAA, passOpti=1)]
						with locked_tools:
							output_manager.WriteOutput.write_results_tally_format(self.tally_input,str(keys), scored_results)
							output_manager.WriteOutput.write_scoring_results(self.scoring_output,str(keys), scored_results)
				with locked_tools:
					output_manager.WriteOutput.write_error(self.error_filename, fastally_res)
				#Reset error list for new file
				fastally_res.reset_error_list()
		except:
			print("Critical error")
		finally:
			self.delete_splitted_files()
			return True

	def get_line_number(self, file_name):
		"""
		-Get fastally sequence number
		-Return number(int) of sequences
		"""
		num_lines=0
		with open(file_name, 'r') as chosen_file:
			for line in chosen_file:
				if line.startswith("#"):
					num_lines += 1
		return num_lines

	def split_fastally_file(self,p_input_file, split_file=10):
		"""
		-Divide user input file.
		-File will be split in 10 parts if it is possible.
		-So maximum thread number is 10.
		Remark : If input file is huge (>10GB), you should increase splitting size.
		Otherwise, it may produce memory crash
		"""
		nb_line=self.get_line_number(p_input_file)
		#If file contains less than 10 sequences, file will not be splitted
		split_size =math.ceil(int(nb_line/split_file))+1
		filename_splitted_file = str(uuid.uuid4())[0:6] 
	   
		input_file = open(p_input_file, 'r')
	   
		count = 0
		counter=0
		file_index = 0
		stop=True
		dest = None
		for line in input_file:
			#Directory to send file + unique filename concatenated with file index and suffix extension '.tmp_fastally'
			path_to_split=Path(self.chosen_dir) / Path(str(filename_splitted_file) + str(file_index) + str('.tmp_fastally'))
			if line.startswith("#"):
				counter+=1
				stop=False
			if count==0:
				if dest: dest.close()
				dest = open(Path(path_to_split), 'w')
				file_index += 1   
			if counter % split_size == 0 and stop==False:
				if dest: dest.close()
				dest = open(Path(path_to_split), 'w')
				file_index += 1
				stop=True
			dest.write(line)
			count += 1
		input_file.close()
		if dest: dest.close()

	def delete_splitted_files(self):
		"""
		-Delete intermediate splitted files
		"""
		regexFile=Path(self.chosen_dir).glob("*.tmp_fastally")
		for filepath in regexFile:
			if(Path.exists(filepath)):
				Path(filepath).unlink()
		

	def launch_multiprocessed_fastally(self):
		"""
		-Launch start_fastally() with multiprocessing
			Input file is splitted in several parts.
			Every process will use splitted files.
		"""
		result_lock = Lock()
		job_queue = JoinableQueue()
		#Loop the list which contains user files. If file is valid, split file into several parts.
		for p_file in self.file_list:
			file_is_valid=parser_manager.Parser.file_validation(p_file)
			if(file_is_valid):
				#Split file into several parts
				self.split_fastally_file(p_file)
				regexFile=Path(self.chosen_dir).glob("*.tmp_fastally")
				#Add splitted files inside Queue
				for filepath in regexFile:
					job_queue.put(str(filepath))
				processes = []
				#Launch multiprocessing	
				for i in range(self.thread_nb):
					t = Process(
					target=self.start_fastally,
					args=(job_queue ,result_lock)
					)
					t.daemon = True
					processes.append(t)
					t.start()
				for t in processes:
					t.join()	
			else:
				#Write error message
				file_error_msg=str(Path(p_file).as_posix() + " : can't open file" )
				print(file_error_msg)
				with open(self.error_filename, 'a') as error_file:
					error_file.write(str(file_error_msg) + '\n')
