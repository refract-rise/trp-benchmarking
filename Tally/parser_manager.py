# --------------------------- Platform imports ---------------------------------
import re
import os.path

#  ---------------------------- External imports ------------------------------
from Bio import SeqIO
from Bio import AlignIO
from collections import defaultdict


#  ---------------------------- External python files ------------------------------
import input_manager


fastally_regex = re.compile(r"^(#[^\n\r]+[\n\r])([A-z?\-\n\r]+)", re.MULTILINE)

class Parser:
	"""
	Parser class is used to store all parser.
	"""
	
	@staticmethod
	def file_validation(p_file):
		"""
		Check if file can be opened
		"""
		validation=False
		if os.path.isfile(p_file) and os.access(p_file, os.R_OK):
			validation=True
		return validation


	@staticmethod
	def clustal_parser(p_file):
		"""
		Parse clustal format with biopython library
		"""
		#Create input object
		clustal_obj = input_manager.MsaInput()
		try:
			alignments = SeqIO.parse(str(p_file), "clustal")
			for align in alignments:
				cl_repeat=str(align.seq)
				clustal_obj.set_repeat_list(cl_repeat)
		except:
			error=str(os.path.basename(p_file)+" : clustal format error")
			clustal_obj.set_error_list(error)
		return clustal_obj
	
	@staticmethod
	def fasta_parser(p_file):
		"""
		Parse fasta format with biopython library
		"""
		#Create input object
		fasta_obj = input_manager.MsaInput()
		try:
			alignments = AlignIO.read(str(p_file), "fasta")
			for align in alignments:
				cl_repeat=str(align.seq)
				fasta_obj.set_repeat_list(cl_repeat)
		except:
			error=str(os.path.basename(p_file)+" : fasta format error")
			fasta_obj.set_error_list(error)
		return fasta_obj


	@staticmethod
	def fastally_parser(p_file):
		"""
		-Parse fastally input format
		-Store results into the dictionary. (key -> str(header) | Value -> List[str(repeat1),str(repeat2),...,str(repeatn)])
		-Send dictionary to an object (MsaInput object), where validation steps will be done
		"""
		#Create input object
		fastally_obj = input_manager.MsaInput()
		#Defaultdict give better performance in this case.
		res_dict = defaultdict()
		counter=1
		with open (p_file, "r",encoding="UTF-8") as myfile:
			#Get all file content as string (data are stored in RAM)
			all_lines = myfile.read()
			#Applied regex on file content and store all match in matches list
			matches = [match.groups() for match in fastally_regex.finditer(all_lines)]
			for element in matches:
				header=str(element[0])
				list_of_MSA=element[1].split()
				#Check if there is no special char in repeat sequence. (see isalpha function)
				valid_char=all(x.replace("-",'').strip('\n').strip().isalpha() for x in list_of_MSA)
				#Check if header or list_of_MSA is not empty
				if(header and list_of_MSA and valid_char):
					header=header.rstrip('\n')
					#Check if key is not already in dict.
					if header not in res_dict:
						res_dict[header] = list_of_MSA
					else:
						old_header=header
						header=header+"_"+str(counter)
						res_dict[header] = list_of_MSA
						counter+=1
						error=str(old_header+" : Header is already present. Header has been replaced by: "+header)
						fastally_obj.set_error_list(error)
				else:
					error=str(header.rstrip('\n')+" : Invalid special char in MSA")
					fastally_obj.set_error_list(error)			
		fastally_obj.set_fastally_format(res_dict)
		return fastally_obj
