#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys, re, os
import argparse
import time
import uuid
from pathlib import Path

#Files modules
import output_manager
import launcher 

def main(argv):
	
	#Initialise Launcher object
	launcher_obj= launcher.Launcher()
	
	#User choices
	get_args = argparse.ArgumentParser()
	get_args.add_argument("msa_files", help="valid msa files", nargs='+')
	get_args.add_argument("-format", help="choose an input format",default="fastally", choices=["clustal", "fastally","fasta"])
	get_args.add_argument("-out", help="Name of unique result file. Example : output.csv. The default value is a random name.")
	get_args.add_argument('-d', '--result_directory', required=True, help='Send result in specified directory. Example : /home/user/result/')
	get_args.add_argument("-q", '--quiet',  help="Do not display any information in terminal", action="store_true")
	get_args.add_argument("-t", '--thread',  help="Thread number", default="1")

	args = get_args.parse_args()

	##########################################
	#
	#			Check user options
	#
	##########################################
	#Type of input format (fastally, fasta, clustal)
	if(args.format): type_parser=args.format
	
	#Directory to send result	
	output_directory=Path(args.result_directory)
	if not output_directory.exists(): sys.exit("Invalid directory : "+str(output_directory))
	if not os.access(output_directory, os.W_OK) : sys.exit("Path directory error. Check write permission")
	
	#Choice of output (result) file name
	if(args.out):
		#Check output file name. Example : output.csv
		filename_regex = re.compile(r'^[\w,\s-]+\.[A-z]+$', re.MULTILINE).match(str(args.out))
		if not filename_regex: sys.exit("Invalid filename : "+str(args.out))
		
		result_output_filename=str(args.out)
		error_report_filename="error_report_"+str(args.out)
		#Path to send results files
		result_path=output_directory / result_output_filename
		error_filename=output_directory / error_report_filename
	else:
		#Default result name
		result_output_filename=str("results_"+launcher_obj.unique_filename+".csv")
		error_report_filename=str("error_report_"+launcher_obj.unique_filename+".txt")
		#Path to send results files
		result_path=output_directory / result_output_filename
		error_filename=output_directory /error_report_filename
	
	#Quiet mode activation
	quiet_mode=True if(args.quiet) else False
	
	#Thread
	if not str(args.thread).isnumeric() : sys.exit("Thread option error")
	if int(args.thread)==0 : sys.exit("Thread option error") 
	thread_nb=int(args.thread) if(args.thread) else int(1)
	
	#Store user files inside list
	file_list=[]
	for files in args.msa_files:
		files=Path(files).resolve()
		file_list.append(files)
	
	#Set data inside object attribute
	launcher_obj.file_list=file_list
	launcher_obj.type_parser=type_parser
	launcher_obj.error_filename=error_filename
	launcher_obj.result_path=result_path
	launcher_obj.quiet_mode=quiet_mode
	launcher_obj.thread_nb=thread_nb
	launcher_obj.chosen_dir=output_directory
	launcher_obj.initialize_tally_path()
		

	##########################################
	#
	#			Launch pipeline
	#
	##########################################
	
	#Write output file with header only. Result data will be added after	
	output_manager.WriteOutput.create_results_file(launcher_obj.tally_input,launcher_obj.scoring_output)
	
	if quiet_mode==False : print("======= Scoring data ======")
	
	#Launch adequate pipeline
	launcher_obj.launch_pipeline()

	if quiet_mode==False : print("======= Tally calculation ======= ")
	
	#Launch tally java program with run_tally function
	launcher_obj.run_tally()
	
	if quiet_mode==False : print("======= Done ======= ")

	
if __name__ == "__main__":
	main(sys.argv[1:])


