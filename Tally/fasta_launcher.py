#  ---------------------------- External imports ------------------------------
from pathlib import Path
from multiprocessing import Process, JoinableQueue, Lock

#  ---------------------------- External python files ------------------------------
import parser_manager
import output_manager
import input_manager
from scoring import repeat_info


class Fasta:
	"""
	Fasta is parent class of Launcher classes
	This class is used to launch pipeline on fasta format with multiprocessing  
	"""
	
	def start_fasta(self,job_queue,result_lock):
		"""
		-Launch fasta parser
		-Scoring data
		-Write results and error report 
		"""
		try:
			while not job_queue.empty():
				filename=job_queue.get()
				#Display score step if quiet mode is not active
				if self.quiet_mode==False : print(Path(filename).stem)
				#Call a function from parser_manager.py. This function returns an MsaInput object.
				fasta_res_obj=parser_manager.Parser.fasta_parser(filename)
				#Check if list is not empty. (case of format error)
				if(fasta_res_obj.get_repeat_list()):
					#IUPAC sequence validation. 
					fasta_res_obj.validation_others_format(filename)
					fasta_repeat_list=fasta_res_obj.get_repeat_list()
					if(len(fasta_repeat_list)>=2):
						fasta_res_obj.compute_count_AA(fasta_repeat_list)
						countAA=fasta_res_obj.get_count_AA()
						scored_results = [repeat_info.Repeat(msa=fasta_repeat_list,
							begin=1,calc_score = True, calc_pValue = True, end=countAA, passOpti=1)]
						with result_lock:
							output_manager.WriteOutput.write_results_tally_format(self.tally_input,str(filename), scored_results)
							output_manager.WriteOutput.write_scoring_results(self.scoring_output,str(filename), scored_results)
				with result_lock:
					output_manager.WriteOutput.write_error(self.error_filename, fasta_res_obj)
				fasta_res_obj.reset_error_list()
				job_queue.task_done()
		except:
			job_queue.task_done()
			

	def launch_multiprocessed_fasta(self):
		"""
		-Launch start_fasta() with multiprocessing
			User inputs files will be launched in several distinct processes 
		"""
		result_lock = Lock()
		job_queue = JoinableQueue()
		#Loop the list which contains user files. If file is valid, add file in queue.				
		for filepath in self.file_list:
			file_is_valid=parser_manager.Parser.file_validation(filepath)
			if(file_is_valid):
				job_queue.put(str(filepath))
			else:
				file_error_msg=str(Path(filepath).as_posix() + " : can't open file" )
				with open(self.error_filename, 'a') as error_file:
					error_file.write(str(file_error_msg) + '\n')
		#Launch multiprocessing on start_fasta()
		for i in range(self.thread_nb):
			t = Process(
			target=self.start_fasta,
			args=(job_queue ,result_lock)
			)
			t.daemon = True
			t.start()
		job_queue.join()	



